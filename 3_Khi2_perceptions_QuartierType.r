################
##  CC BY-NC  ##
################

# à la suite du dataframe "q" du prog 2_croisements_graphiques.r


########## TESTS DU KHI2 avec quartier ##########

# L'idée est de tester si les perceptions sont différentes selon les types de quartier : d'intervention ou de contrôle


# plusieurs fois "Chi-squared approximation may be incorrect"
# a priori, pas assez d'individus dans chq case, mais à vérifier

levels(q$AIR_EXT)
chisq.test(q$Quartier_type,q$AIR_EXT)
# sans prendre en compte les réponses NSP : 
chisq.test(q[q$AIR_EXT != "Je ne sais pas",]$Quartier_type,q[q$AIR_EXT != "Je ne sais pas",]$AIR_EXT)

# pour "régler" le pb du nbre :
chisq.test(q$Quartier_type,q$AIR_EXT, simulate.p.value = TRUE)
chisq.test(q[q$AIR_EXT != "Je ne sais pas",]$Quartier_type,q[q$AIR_EXT != "Je ne sais pas",]$AIR_EXT, simulate.p.value = TRUE)

chisq.test(q$Quartier_type,q$AIR_EXT2)
chisq.test(q[q$AIR_EXT2 != "Je ne sais pas",]$Quartier_type,q[q$AIR_EXT2 != "Je ne sais pas",]$AIR_EXT2)

levels(q$AIR_INT)
chisq.test(q$Quartier_type,q$AIR_INT)
chisq.test(q[q$AIR_INT != "Je ne sais pas",]$Quartier_type,q[q$AIR_INT != "Je ne sais pas",]$AIR_INT)

chisq.test(q$Quartier_type,q$AIR_INT2)
chisq.test(q[q$AIR_INT2 != "Je ne sais pas",]$Quartier_type,q[q$AIR_INT2 != "Je ne sais pas",]$AIR_INT2)

levels(q$BRUIT_EXT)
chisq.test(q$Quartier_type,q$BRUIT_EXT)
chisq.test(q[q$BRUIT_EXT != "Je ne sais pas",]$Quartier_type,q[q$BRUIT_EXT != "Je ne sais pas",]$BRUIT_EXT)

chisq.test(q$Quartier_type,q$BRUIT_EXT)
chisq.test(q$Quartier_type,q$BRUIT_EXT2, simulate.p.value = TRUE)
chisq.test(q[q$BRUIT_EXT != "Je ne sais pas",]$Quartier_type,q[q$BRUIT_EXT != "Je ne sais pas",]$BRUIT_EXT)
chisq.test(q[q$BRUIT_EXT2 != "Je ne sais pas",]$Quartier_type,q[q$BRUIT_EXT2 != "Je ne sais pas",]$BRUIT_EXT2, simulate.p.value = TRUE)

levels(q$BRUIT_INT)
chisq.test(q$Quartier_type,q$BRUIT_INT)
chisq.test(q$Quartier_type,q$BRUIT_INT, simulate.p.value = TRUE)
chisq.test(q[q$BRUIT_INT != "Je ne sais pas",]$Quartier_type,q[q$BRUIT_INT != "Je ne sais pas",]$BRUIT_INT)

chisq.test(q$Quartier_type,q$BRUIT_INT2)
chisq.test(q$Quartier_type,q$BRUIT_INT2, simulate.p.value = TRUE)
chisq.test(q[q$BRUIT_INT2 != "Je ne sais pas",]$Quartier_type,q[q$BRUIT_INT2 != "Je ne sais pas",]$BRUIT_INT2)


