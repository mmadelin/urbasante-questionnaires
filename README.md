# UrbASanté-questionnaires



## Descriptif

Dans le cadre de l'[ANR UrbASanté](https://urbasante.fr/), les habitant·es de plusieurs quartiers du 18ème arrondissement de Paris ont été questionné·es sur plusieurs volets (alimentation, mobilité...), en distinguant des quartiers dits d'intervention de ceux de contrôle.  

Voici les programmes utilisés pour l'exploitation des données des réponses au questionnaire et en particulier sur le volet des nuisances environnementales (qualité de l'air et bruit).  

Les réponses au questionnaire ne sont pas disponibles ici.

## Organisation des programmes

1/ pour préparer les tableaux de données (importation, réorganisation, recodage, etc.)   

2/ premiers traitements, avec les croisements d'une variable avec les perceptions, et les graphiques associés  
Rmq : le fichier ne comporte plus de NA pour les perceptions ou le quartier.  

3/ tests du Khi2 entre les niveaux de perception et les types de quartier (intervention/contrôle)  

## À suivre...

En fonction des discussions de la réunion ANR du 10/10/2024, nouveau df a priori.
