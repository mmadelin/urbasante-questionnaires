################
##  CC BY-NC  ##
################

library(readxl)
library(dplyr)

############### IMPORT DES FICHIERS ###############

# Ouverture des différents fichiers du questionnaire (= les différents volets)
p1 <- read_excel("Questionnaires/QUEST_PARTIE1.xlsx")
p2 <- read_excel("Questionnaires/QUEST_PARTIE2.xlsx")
p3 <- read_excel("Questionnaires/QUEST_PARTIE3.xlsx")
p4 <- read_excel("Questionnaires/QUEST_PARTIE4.xlsx")
p5 <- read_excel("Questionnaires/QUEST_PARTIE5.xlsx")

# Lecture des quartiers / IRIS / Carreaux des enquêté·es et sélection des infos intéressantes (code de l'enquêté·e, quartiers et découpages INSEE)
qu <- read_excel("Questionnaires/quartier_variableok_clean(1).xlsx")
qu <- qu %>% select(hashcode,
                    Quartier_nom, Quartier_type , 
                    IRIS, carfid
)

################ SELECTION INFOS ################

# Pour chq volet, (1) SELECTION des infos intéressantes a priori, (2) RENOMMAGE des variables (plus court, parfois plus explicite a priori) + premières vérifications des fichiers
p1_select <- p1 %>% select(
  hashcode,
  ident=comment_vous_identifiez_vous,
  naissance = quelle_est_votre_annee_de_naissa, 
  depuisqd = depuis_combien_de_temps_vivez_vo,
  diplome = quel_est_le_diplome_le_plus_elev,
  revenus = dans_la_liste_ci_dessous_cochez_,
  sentimentfric = financierement_dans_votre_foyer_,
  statutocc = quel_est_le_statut_doccupation_d,
  enfants = actuellement_parmi_les_personne1,
  situationpro = quelle_est_votre_situation_actue
)
table(p1_select$ident)
table(p1$Quartier_nom, p1$Quartier_type)

p2_select <- p2 %>% select(
  hashcode,
  choix_cadre_vie_quartier = quelles_sont_la_ou_les_principa3,
  changequartier = laquelle_des_phrases_suivantes_c,
  changement_du_quartier = selon_vous_sur_une_echelle_de_11,
  pb_gensbruyants = selon_vous_dans_votre_quartier_l, 
  espvert = au_sein_de_votre_quartier_a_quel,
  equipsportifs = au_sein_de_votre_quartier_a_que2,
  assoquartiers = au_sein_de_votre_quartier_a_que3,
)
# chgt de quartier (avec regroupement à faire) : laquelle_des_phrases_suivantes_c
table(p2$si_oui_lesquels)

p3_select <- p3 %>% select(
  hashcode, 
  sante=dans_lensemble_pensez_vous_que_v,
  effortphys = realiser_des_efforts_physiques_m,
  escaliers = monter_plusieurs_etages_par_lesc,
  sifflementspoitrine = avez_vous_eu_des_sifflements_dan,
  siff_infectionrespi = lesquels_de_ces_facteurs_aont_da,
  siff_tabac = lesquels_de_ces_facteurs_aont_d2,
  siff_pollen = lesquels_de_ces_facteurs_aont_d6,
  siff_poussieres = lesquels_de_ces_facteurs_aont_d7,
  siff_pollair = lesquels_de_ces_facteurs_aont_d9,
  siff_emanation = lesquels_de_ces_facteurs_aont_11, 
  generespi = vous_etes_vous_reveillee_avec_un,
  essoufflement = avez_vous_eu_une_crise_dessouffl,
  nez = avez_vous_eu_des_problemes_deter, 
  yeux = avez_vous_eu_les_yeux_qui_piquai,
  fumeur_se = fumez_vous_actuellement,
  avec_fumeur_se = actuellement_habitez_vous_en_com
)

p5_select <- p5 %>% select(
  hashcode, 
  marche_course = quels_estsont_les_modes_de_trans,
  velo_course = quels_estsont_les_modes_de_tran1,
  deuxroues_course = quels_estsont_les_modes_de_tran2,
  transpcomm_course = quels_estsont_les_modes_de_tran3,
  voiture_course = quels_estsont_les_modes_de_tran4,
  marche_activite = lors_de_ces_deplacements_pour_vo,
  velo_activite = lors_de_ces_deplacements_pour_v1,
  deuxroues_activite = lors_de_ces_deplacements_pour_v2,
  transpcomm_activite = lors_de_ces_deplacements_pour_v3,
  voiture_activite = lors_de_ces_deplacements_pour_v4,
  marche_loisirs = quels_estsont_les_modes_de_trans,
  velo_loisirs = quels_estsont_les_modes_de_tran1,
  deuxroues_loisirs = quels_estsont_les_modes_de_tran2,
  transpcomm_loisirs = quels_estsont_les_modes_de_tran3,
  voiture_loisirs = quels_estsont_les_modes_de_tran4,
)

p4_select <- p4 %>% select(
  hashcode,
  AIR_EXT = comment_jugez_vous_globalement_l,
  AIR_INT = a_linterieur_de_votre_logement_c,
  BRUIT_EXT = comment_jugez_vous_globalement_1,
  BRUIT_INT = comment_jugez_vous_la_qualite_d9)


############### PERCEPTION + OU - ###############

# création de 4 nouvelles variables avec 2 blocs : avis positif et avis négatif 
p4_select <- p4_select %>% 
  mutate(AIR_EXT2 = ifelse(AIR_EXT == "Très satisfaisante" | AIR_EXT == "Satisfaisante", "positif",
                    ifelse(AIR_EXT == "Peu satisfaisante" | AIR_EXT == "Pas du tout satisfaisante" , "négatif", AIR_EXT)),
         AIR_INT2 = ifelse(AIR_INT == "Très satisfaisante" | AIR_INT == "Satisfaisante", "positif",
                           ifelse(AIR_INT == "Peu satisfaisante" | AIR_INT == "Pas du tout satisfaisante" , "négatif", AIR_INT)),
         BRUIT_EXT2 = ifelse(BRUIT_EXT == "Très satisfaisante" | BRUIT_EXT == "Satisfaisante", "positif",
                           ifelse(BRUIT_EXT == "Peu satisfaisante" | BRUIT_EXT == "Pas du tout satisfaisante" , "négatif", BRUIT_EXT)),
         BRUIT_INT2 = ifelse(BRUIT_INT == "Très satisfaisante" | BRUIT_INT == "Satisfaisante", "positif",
                           ifelse(BRUIT_INT == "Peu satisfaisante" | BRUIT_INT == "Pas du tout satisfaisante" , "négatif", BRUIT_INT))         )

############# JOINTURE DES PARTIES ##############
q <- full_join(p4_select, p1_select, by = "hashcode")
q <- full_join(q, p2_select, by = "hashcode")
q <- full_join(q, p3_select, by = "hashcode")
q <- full_join(q, p5_select, by = "hashcode")
q <- full_join(qu,q,  by = "hashcode")

############ RECODAGE ET REGROUPEMENT ###########

# recodage et changement de l'ordre des facteurs :
# à chaque fois si regroupement ou recodage, création d'une nouvelle variable (avec 2 à la fin)

ordre_satisf <- c("Pas du tout satisfaisante", "Peu satisfaisante","Satisfaisante", "Très satisfaisante","Je ne sais pas")
q$AIR_EXT <- factor(q$AIR_EXT, levels = ordre_satisf)
q$AIR_INT <- factor(q$AIR_INT, levels = ordre_satisf)
q$BRUIT_EXT <- factor(q$BRUIT_EXT, levels = ordre_satisf)
q$BRUIT_INT <- factor(q$BRUIT_INT, levels = ordre_satisf)

ordre_satisf <- c("négatif","positif","Je ne sais pas")
q$AIR_EXT2 <- factor(q$AIR_EXT2, levels = ordre_satisf)
q$BRUIT_INT2 <- factor(q$BRUIT_INT2, levels = ordre_satisf)
q$AIR_INT2 <- factor(q$AIR_INT2, levels = ordre_satisf)
q$BRUIT_EXT2 <- factor(q$BRUIT_EXT2, levels = ordre_satisf)
levels(q$BRUIT_EXT2)

q$sentimentfric2 <- factor(q$sentimentfric, levels= rev(c("Vous ne pouvez pas y arriver sans faire de dettes", "Vous y arrivez difficilement","C’est juste","Ça va","Vous êtes à l’aise", "Ne souhaite pas répondre")))

q <- q %>% 
  mutate(statutocc2 = ifelse(statutocc == "Hébergement par de la famille ou des amis", "Chez famille/amis",
                           ifelse(statutocc == "Hébergement par prise en charge sociale (hôtel, samu social, foyer, etc...)","Prise charge sociale",
                                  statutocc)))
q$statutocc2 <- factor(q$statutocc2, levels=c("Propriétaire", "Locataire", "Sous-locataire", "Chez famille/amis" , "Prise charge sociale"))

q$depuisqd2 <- factor(q$depuisqd, levels=c("Moins de 6 mois", "6-12 mois", "De 1 à 5 ans", "De 5 à 10 ans" , "Plus de 10 ans"))

q <- q %>% 
  mutate(enfants2 = ifelse(enfants == "0", "Pas d'enfant <13ans","Enfant(s) <13ans"))

q <- q %>% 
  mutate(fumeur_se2 = ifelse(fumeur_se == "Non, mais j’ai déjà fumé" | fumeur_se == "Non, je n’ai jamais fumé", "Non fumeur_se", "Fumeur_se"))

q <- q %>% 
  mutate(fumeur_avecfum = paste0(fumeur_se2, ifelse(avec_fumeur_se == "Oui", ",\navec fumeur_se", "")))
unique(q$fumeur_avecfum)

unique(q$avec_fumeur_se)
q <- q %>% 
  mutate(fumeur_se3 = paste(fumeur_se2 == "Non fumeur_se", "Fumeur_se"))

q <- q %>% 
  mutate(situationpro2 = ifelse(situationpro == "Retraité(e)","Retraité(e)", "Non Retraité(e)"))

q <- q %>% 
  mutate(choix_cadre_vie_quartier = ifelse(is.na(choix_cadre_vie_quartier) == TRUE,"Non", "Un des critères"))

q <- q %>% 
  mutate(changequartier2 = ifelse(changequartier == "Je ne souhaite pas changer de quartier", "Ne souhaite pas",
                             ifelse(changequartier == "Je souhaiterais changer de quartier, mais je ne peux pas pour d’autres raisons (familiales, professionnelles, etc.)" | changequartier == "Je souhaiterais changer de quartier, mais je ne peux pas pour des raisons financières" | changequartier == "Je souhaite changer de quartier, et je prévois de le faire à court ou moyen terme",
                                    "Souhaite changer",
                                    changequartier)))

q <- q %>% 
  mutate(pb_gensbruyants2 = ifelse(pb_gensbruyants == "Assez fréquents" | pb_gensbruyants == "Très fréquents", "Assez/très fréquent",
                                   "Pas/peu fréquent"))

q$changement_du_quartier <- as.numeric(q$changement_du_quartier)
q <- q %>% 
  mutate(changement_du_quartier2 = ifelse(changement_du_quartier <5,
                                          "Plutôt non",
                                          ifelse(changement_du_quartier >6,
                                                 "Plutôt oui", "Moyen")))
table(q$changement_du_quartier2)
q$changement_du_quartier2 <- factor(q$changement_du_quartier2, levels=c("Plutôt non", "Moyen", "Plutôt oui"))

q <- q %>% 
  mutate(sante_3K = ifelse(sante == "Médiocre" | sante == "Mauvaise" , 
                           "Médiocre/Mauvaise", 
                                          ifelse(sante == "Très bonne" | sante == "Excellente", "Très bonne/Excellente", sante)))

q$sante_3K <- factor(q$sante_3K, levels=c("Médiocre/Mauvaise", "Bonne","Très bonne/Excellente"))

q$sante <- factor(q$sante, levels=c("Médiocre", "Mauvaise", "Bonne","Très bonne", "Excellente"))

q$effortphys <- factor(q$effortphys, levels=c("Non, pas du tout limité", "Oui, un peu limité", "Oui, beaucoup limité"))
q$escaliers <- factor(q$escaliers, levels=c("Non, pas du tout limité", "Oui, un peu limité", "Oui, beaucoup limité"))


q <- q %>% 
  mutate(revenus2 = ifelse(revenus == "Moins de 600€ par mois (moins de 7200€ par an)", "< 600€/mois",
                           ifelse(revenus == "De 600€ à moins de 1302€ par mois (de 7200€ à moins de 15624€ par an)","600-1300€/mois",
                                  ifelse(revenus == "De 1302€ à moins de 2000€ par mois (de 15624€ à moins de 24000€ par an)","1300-2000€/mois",
                                         ifelse(revenus == "De 2000€ à moins de 3000€ par mois (de 24000€ à moins de 36000€ par an)","2000-3000€/mois",
                                                ifelse(revenus == "Plus de 3000€ par mois (plus de 36000€ par an)","> 3000€/mois", revenus))))))

q$revenus2 <- factor(q$revenus2, levels = rev(c("< 600€/mois",
                                                "600-1300€/mois",
                                                "1300-2000€/mois",
                                                "2000-3000€/mois",
                                                "> 3000€/mois",
                                                "Ne souhaite pas répondre",
                                                "Ne sait pas")))
levels(q$revenus2)

q <- q %>% 
  mutate(naissance2 = ifelse(as.numeric(naissance) < 1950, "Avant 1950",
                             ifelse(as.numeric(naissance) < 1970,"1950-1970",
                                    ifelse(as.numeric(naissance) < 1990,"1970-1990", "Après 1990"))))
q$naissance2 <- factor(q$naissance2, levels = c("Avant 1950", "1950-1970", "1970-1990", "Après 1990"))

q$age <- 2023 - as.numeric(q$naissance)
q <- q %>% 
  mutate(classe_age = ifelse(q$age < 40, "18-39 ans",
                             ifelse(q$age < 65,"40-64 ans", "+ 65 ans")))
q$classe_age <- factor(q$classe_age, levels = c("18-39 ans", "40-64 ans", "+ 65 ans"))

rm(ordre_satisf)
saveRDS(q, "reponses_questionnaires.RDS")

# pas fini, réflexion sur des types de mobilité
# vélomarche : nbre de fois courses, activité et loisirs total ou séparé => en fonction de la distribution
# cf tableau
# march_agreable_quotid : selon_vous_est_il_agreable_de_ma
# march_agreable_loisirs selon_vous_est_il_agreable_de_m1
