################
##  CC BY-NC  ##
################

# après avoir exécuté le prog 1_prepa_tab

############## TRAITEMENTS EN VRAC ##############

# pour avoir les % de perceptions + ou -
round(prop.table(table(q$AIR_EXT2)),3)*100
round(prop.table(table(q$AIR_INT2)),3)*100
round(prop.table(table(q$BRUIT_EXT2)),3)*100
round(prop.table(table(q$BRUIT_INT2)),3)*100

table( q$Quartier_type, q$IRIS)
table( q$Quartier_type, q$carfid)
table( q$Quartier_nom, q$IRIS)
table( q$Quartier_nom, q$carfid)

# j'enlève les individus n'ayant pas de réponse pour les perceptions et les "Missing"
q <- q %>% filter(!is.na(BRUIT_EXT))
q <- q %>% filter(!is.na(AIR_INT))
table(q$Quartier_nom)
q <- q %>% filter(Quartier_nom != "Missing")
table(q$Quartier_nom)

# Exploration des croisements de variable
# notamment pour préparer les figures du diaporama de la réunion du 10/10/24
a <- table( q$Quartier_nom, q$ident)
b <- prop.table(a,1)
b <- b*100
round (b, 0) 

a <- table( q$Quartier_type, q$ident)
b <- prop.table(a,1)
b <- b*100
round (b, 1) 

a <- table( q[q$ident != "Non binaire",]$Quartier_nom, q[q$ident != "Non binaire",]$ident)
b <- prop.table(a,1)
b <- b*100
round (b, 0) 

a <- table( q$Quartier_nom, q$classe_age)
b <- prop.table(a,1)
b <- b*100
round (b, 1) 

a <- prop.table(table(q$ident))
a <- a*100
a

a <- table( q$Quartier_type, q$statutocc2)
b <- prop.table(a,1)
b <- b*100
round (b, 0) 

mosaicplot(a)

# histogramme des classes d'âge pour décider des grandes catégories pour la comparaison avec les IRIS / carreaux 
a <- q %>% filter(Quartier_nom =="Chapelle International")
hist(2023 - as.numeric(a$naissance), 10)
quantile(2023 - as.numeric(a$naissance))


library(ggplot2)
library(geomtextpath)

ggplot(q, aes(x = as.numeric(naissance), colour = Quartier_type, label = Quartier_type)) +
  geom_textdensity(hjust = 0.1) +
  theme_bw() + guides(color = 'none')



############ QUESTIONS -> GRAPHIQUES ############

a <- table(q$AIR_EXT)
a <- prop.table(a)
a <- round(a*100,0)
a

a <- table(q$AIR_EXT)
a <- prop.table(a)
a <- a*100

# ajout de la variable à croiser avec les perceptions => "var", pour refaire tourner le programme
var <- q$ident
dev.off()

a <- table( q$AIR_EXT, var)
b <- prop.table(a,2)
b <- b*100
b <- round (b, 0) 
b
c <- table( q$AIR_INT, var)
d <- prop.table(c,2)
d <- d*100
d <- round (d, 0) 
d

# 2 colonnes
par(mfrow = c(2, 2), oma = c(2, 6, 0, 0))
col <- c( "#0076BA","#7fbadc", "#f1cce3", "#ce4c9f", "grey")
barplot(a, col = col, horiz = TRUE, 
        cex.names = 1, cex.axis = 1, las=1)
mtext("En nombre", side=1, line=2, cex = 0.8)
title("EXTÉRIEUR")
barplot(b, col = col, horiz = TRUE, 
        cex.names = 0.01, cex.axis = 1, las=1)
abline(v=50,col = "gray30", lwd=1)
abline(v=69.4,col = "gray30", lty = 2, lwd=3)
mtext("En %", side=1, line=2, cex = 0.8)
col2 <- c( "#0076BA","#7fbadc", "#f1cce3", "#ce4c9f", "grey")
barplot(c, col = col2, horiz = TRUE, 
        cex.names = 1, cex.axis = 1, las=1)
mtext("En nombre", side=1, line=2, cex = 0.8)
title("INTÉRIEUR")
barplot(d, col = col2, horiz = TRUE, 
        cex.names = 0.01, cex.axis = 1, las=1)
abline(v=50,col = "gray30", lwd=1)
abline(v=32.1,col = "gray30", lty = 2, lwd=3)
mtext("En %", side=1, line=2, cex = 0.8)


###
# BRUIT ###

a <- table( q$BRUIT_EXT, var)
b <- prop.table(a,2)
b <- b*100
b <- round (b, 0) 
b
c <- table( q$BRUIT_INT, var)
d <- prop.table(c,2)
d <- d*100
d <- round (d, 0) 
d

# 2 colonnes
par(mfrow = c(2, 2), oma = c(2, 6, 0, 0))
col <- c( "#568940","#b4caaa", "#f3cda6", "#e38421", "grey")
barplot(a, col = col, horiz = TRUE, 
        cex.names = 1, cex.axis = 1, las=1)
mtext("En nombre", side=1, line=2, cex = 0.8)
title("EXTÉRIEUR")
barplot(b, col = col, horiz = TRUE, 
        cex.names = 0.01, cex.axis = 1, las=1)
abline(v=50,col = "gray30", lwd=1)
abline(v=72.2,col = "gray30", lty = 2, lwd=3)
mtext("En %", side=1, line=2, cex = 0.8)
col <- c( "#568940","#b4caaa", "#f3cda6", "#e38421", "grey")
barplot(c, col = col2, horiz = TRUE, 
        cex.names = 1, cex.axis = 1, las=1)
mtext("En nombre", side=1, line=2, cex = 0.8)
title("INTÉRIEUR")
barplot(d, col = col2, horiz = TRUE, 
        cex.names = 0.01, cex.axis = 1, las=1)
abline(v=50,col = "gray30", lwd=1)
abline(v=45.7,col = "gray30", lty = 2, lwd=3)
mtext("En %", side=1, line=2, cex = 0.8)

dev.off()


# Comparaison des 2 quartiers
dev.off()

a <- q %>% filter(Quartier_nom == "Charles Hermite")
var1 <- a$revenus2
a <- table( a$AIR_EXT, var1)
b <- prop.table(a,2)
b <- b*100
b <- round (b, 0) 
b
c <- q %>% filter(Quartier_nom == "Chapelle Evangile")
var2  <- c$revenus2
c <- table( c$AIR_EXT, var2)
d <- prop.table(c,2)
d <- d*100
d <- round (d, 0) 
d
par(mfrow = c(2, 2), oma = c(2, 6, 0, 0))
col <- c( "#0076BA","#7fbadc", "#f1cce3", "#ce4c9f", "grey")
barplot(a, col = col, horiz = TRUE, 
        cex.names = 1, cex.axis = 1, las=1)
mtext("En nombre", side=1, line=2, cex = 0.8)
title("CHARLES HERMITE")
barplot(b, col = col, horiz = TRUE, 
        cex.names = 0.01, cex.axis = 1, las=1)
abline(v=50,col = "gray30", lwd=1)
abline(v=69.4,col = "gray30", lty = 2, lwd=3)
mtext("En %", side=1, line=2, cex = 0.8)
col2 <- c( "#0076BA","#7fbadc", "#f1cce3", "#ce4c9f", "grey")
barplot(c, col = col2, horiz = TRUE, 
        cex.names = 1, cex.axis = 1, las=1)
mtext("En nombre", side=1, line=2, cex = 0.8)
title("CHAPELLE EVANGILE")
barplot(d, col = col2, horiz = TRUE, 
        cex.names = 0.01, cex.axis = 1, las=1)
abline(v=50,col = "gray30", lwd=1)
abline(v=69.4,col = "gray30", lty = 2, lwd=3)
mtext("En %", side=1, line=2, cex = 0.8)


# 3 variables

a <- table( q$AIR_INT, var, q$Quartier_type)
b <- prop.table(a,3)
b <- b*100
b <- round (b, 0) 
b
c <- table( q$AIR_INT, var)
d <- prop.table(c,2)
d <- d*100
d <- round (d, 0) 
d

dev.off()
